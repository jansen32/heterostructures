#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"timeevclass.hpp"
#include"lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"htham.hpp"
#include <boost/program_options.hpp>
#include<iostream>

using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{

  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();


  int Llead1{};
  int Llead2{};
  int Lchain{};
  int M;
  std::string filename;
  double gamma{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
    double eps0{};
std::string mpsName{};
 std::string siteSetName{};
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("gam", boost::program_options::value(&gamma)->default_value(0), "gam")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("f", boost::program_options::value(&filename)->default_value("noName"), "f")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName");
    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {     
	// std::cout << "Llead1: " << Llead1 << '\n';

      }
            if (vm.count("Ll2"))
      {   
	//   std::cout << "Llead2: " << Llead2 << '\n';

      }
         if (vm.count("Lc"))
      {   
	//   std::cout << "Lchain: " << Lchain << '\n';

      }
      	     if (vm.count("M"))
      {   
	//   std::cout << "M: " << M << '\n';

      }

    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	
  int N=Llead1+Llead2+Lchain;
  
  std::vector<int> v(N, 0);
  for(int i=Llead1; i<Llead1+Lchain; i++)
    {
      v[i]=M;

    }
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};

  auto sites = itensor::readFromFile<itensor::Holstein>(siteSetName);
  auto psi = itensor::readFromFile<MPS>(mpsName);

  auto H1=toMPO(itensor::makeHetHam(sites, Llead1, Llead2,  Lchain, tl, tint, t0, gamma, omega, eps0 ));
auto E0=itensor::innerC(psi, H1,psi);
 auto args2 = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000);
  auto psiOne=applyMPO(H1,psi, args2);
  psiOne.noPrime();
  auto E0Sqrd=itensor::innerC(psi,H1,psiOne);
 	  auto VAL=E0*E0;
 auto relVARIANCE =  std::abs((E0Sqrd-VAL )/VAL);
 		    std::cout<< "THE REL GS VAR WAS "<< relVARIANCE<<std::endl;
 		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;
    std::ofstream outfile;
    outfile.open(filename, std::ios_base::app); // append instead of overwrite
    outfile <<std::endl<< gamma<<","<<E0<<","<<relVARIANCE; 
    outfile.close();
     return 0;
}
