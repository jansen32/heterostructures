#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"timeevclass.hpp"
#include"lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"htham.hpp"
#include <boost/program_options.hpp>
#include<iostream>

using namespace std;

using namespace itensor;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{



  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();
std::string mpsName{};
 std::string siteSetName{};

 double cutoff{};
  double lbocutoff{};
  int M{};
  int lboMd{};
    int Md{};
  int Llead1{};
  int Llead2{};
  int Lchain{};
  double t0{};
  double tl{};
  double tint{};
  double omega{};
  double gamma{};
   double dt{};
  double tot{};
  double V{};
  double eps0{};

  std::string scutoff{};
  std::string sM{};
  std::string slbocutoff{};
  std::string sMd{};
  std::string slboMd{};
  std::string sLlead1{};
  std::string sLlead2{};
  std::string sLchain{};
  std::string st0{};
  std::string stl{};
  std::string stint{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
  std::string seps0{};
std::string DIR="";
  std::string filename="";
 bool saveNr=true;
 bool saveBasis=true;
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("tl", boost::program_options::value(&tl)->default_value(1.0), "tl")
      ("tint", boost::program_options::value(&tint)->default_value(1.0), "tint")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("eps0", boost::program_options::value(&eps0)->default_value(0.0), "eps0")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(200), "lboMd")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-9), "cut")
      ("saveBasis", boost::program_options::value(&saveBasis)->default_value(false), "saveBasis")
      ("d", boost::program_options::value(&DIR)->default_value("NODIR"), "d");
 

    
  


    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
    if (vm.count("Ll1"))
      {      std::cout << "Llead1: " << Llead1 << '\n';
      	sLlead1="Ll1"+std::to_string(vm["Ll1"].as<int>());
	filename+=sLlead1;
      }
            if (vm.count("Ll2"))
      {      std::cout << "Llead2: " << Llead2 << '\n';
      	sLlead2="Ll2"+std::to_string(vm["Ll2"].as<int>());
	filename+=sLlead2;
      }
         if (vm.count("Lc"))
      {      std::cout << "Lchain: " << Lchain << '\n';
      	sLchain="Lc"+std::to_string(vm["Lc"].as<int>());
	filename+=sLchain;
      }
      	     if (vm.count("M"))
      {      std::cout << "M: " << M << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }
      	 if (vm.count("t0"))
   {      std::cout << "t0: " << t0 << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 5);
      	filename+=st0;
      }
      	 	 if (vm.count("tl"))
      {      std::cout << "tl: " << tl << '\n';
      	stl="tl"+std::to_string(vm["tl"].as<double>()).substr(0, 5);
      		filename+=stl;
      }
      		 if (vm.count("tint"))
      {      std::cout << "tint: " << tint << '\n';
      	stint="tint"+std::to_string(vm["tint"].as<double>()).substr(0, 5);
      		filename+=stint;
      }
      		 if (vm.count("gam"))
      {      std::cout << "gamma: " << gamma << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 5);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << omega << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 5);
      		filename+=somega;
      }
		       	 	 if (vm.count("eps0"))
      {      std::cout << "eps0: " << eps0 << '\n';
      	seps0="eps0"+std::to_string(vm["eps0"].as<double>()).substr(0, 5);
      		filename+=seps0;
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << V << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 6);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << tot << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 4);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << dt << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
		       	     if (vm.count("Md"))
      {      std::cout << "Md: " << Md << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
			           	     if (vm.count("lboMd"))
      {      std::cout << "lboMd: " << lboMd << '\n';
      	slboMd="lboMd"+std::to_string(lboMd);
      	filename+=slboMd;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << cutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["cut"].as<double>();
	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
		       		 if (vm.count("lbocut"))
      {      std::cout << "lbocutoff: " << lbocutoff << '\n';
	 std::stringstream ss;
	 ss<<vm["lbocut"].as<double>();
	 scutoff="lbocut"+ss.str();
      	filename+=scutoff;
      }

 		       		 if (vm.count("d"))
      {      std::cout << "Dirname: " << DIR << '\n';

      }

      
      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  //	filename+=".bin";
  std::cout<<filename<<std::endl;
  auto sites = itensor::readFromFile<itensor::Holstein>(siteSetName);
  auto psi0 = itensor::readFromFile<MPS>(mpsName);
  size_t N=length(psi0);
  std::vector<int> v(N, 0);
  // is stored as vecto so start site-1
for(int i=Llead1; i<Llead1+Lchain; i++)
    {
      v[i]=M;

    }
//"MinDim=",maxLinkDim(psi0)
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "CutoffLBO",lbocutoff  , "MaxDim=",Md, "Normalize",true, "SVDMethod", "gesdd");
  //    auto H2=itensor::makeHetHamV(sites, Llead1, Llead2, Lchain, V/2, tl, tint, t0, gamma, omega, eps0);
  psi0.position(1);
  psi0.normalize();
  std::cout<< "stop 1"<< std::endl;
    auto HV=itensor::makeHetHamV(sites, Llead1, Llead2, Lchain, V/2, tl, tint, t0, gamma, omega, eps0);
    auto H0=itensor::makeHetHamV(sites, Llead1, Llead2, Lchain, 0., tl, tint, t0, gamma, omega, eps0);
auto Curr=MPO(itensor::j(sites, Llead1,Llead2, Lchain, tint ));
  auto CurrE=MPO(itensor::jEdot(sites, Llead1,Llead2, Lchain, tl , V/2));
  auto EL=MPO(itensor::ELdot(sites, Llead1,Llead2, Lchain, tl, V/2 ));
  auto ER=MPO(itensor::ERdot(sites, Llead1,Llead2, Lchain, tl, V/2 ));
  auto EHYB=MPO(itensor::EHYBdot(sites, Llead1,Llead2, Lchain, tint ));
  auto CDWdisp=CDWDispstruct(sites, Llead1, Lchain);
  auto Nst=Nstruct(sites, Llead1, Lchain);
auto CDWO=CDWOstruct(sites, Llead1, Lchain);
 auto NPH=Ephstruct(sites, Llead1, Lchain, omega);
 auto EKIN=Ekinstruct(sites, Llead1, Lchain, t0);
 auto ESTRUCT=Estruct(sites, Llead1, Llead2, Lchain, t0, gamma, omega, eps0);

 auto ECOUP=Ecoupstruct(sites, Llead1,  Lchain, gamma);


        auto ampo = AutoMPO(sites);
	    for(int j=1;j  <=N; j+=1)
        {

      	ampo += 1,"n",j;

	}
	    auto Ntot=MPO(ampo);
  std::map<std::string, double> param;
   std::cout<< "stop "<< std::endl;
 param.insert({"t0",t0});
 param.insert({"tl",tl});
 param.insert({"tint",tint});
 param.insert({"V",V}); // now using -V
 param.insert({"omega",omega});
 param.insert({"gamma",gamma});
 param.insert({"eps0",eps0});
 //  param.insert({"lambda",lambda});


 

  int i=0;


  itensor::IHGdot GM(sites, gates, dt, param, Llead1,Llead2, Lchain);



 

  TimeEvolveLbo< IHGdot, decltype(psi0), false> TE(GM,  psi0, length(psi0),  argsMPS, argsState);
   TE.makeLboStruct(Llead1, Lchain);

      while(dt*i<tot)
     {
       
       auto psir2=TE.getBareMpsStruct(Llead1, Lchain);
       psir2.position(Llead1+1);
       auto NORM=itensor::norm(psir2);
       if(saveBasis and (i%int(1/dt))==0)
	 {
	     	   get_local_basis(sites, psir2,Llead1,Lchain, DIR,M,i*dt);
}

              dens_dens_correl( sites,  psir2,  Llead1, Lchain, DIR);
   auto cdwdisp = (itensor::innerC(psir2, CDWdisp, psir2));
   auto cdwo = (itensor::innerC(psir2, CDWO, psir2));
   auto estruct = (itensor::innerC(psir2, ESTRUCT, psir2));
   auto ekin = (itensor::innerC(psir2, EKIN, psir2));
   auto Nph = (itensor::innerC(psir2, NPH, psir2));
   auto Ne = (itensor::innerC(psir2, Nst, psir2));
   auto c = (itensor::innerC(psir2, Curr, psir2));
   auto el=(itensor::innerC(psir2, EL, psir2));
   auto er=(itensor::innerC(psir2, ER, psir2));
   auto curre= (itensor::innerC(psir2, CurrE, psir2));
   auto ehyb= (itensor::innerC(psir2, EHYB, psir2));

auto ev= (itensor::innerC(psir2, HV, psir2));
   auto e0= (itensor::innerC(psir2, H0, psir2));

   auto ecoup= (itensor::innerC(psir2, ECOUP, psir2));
		  double ne_val=real(Ne);
		  double cdwdisp_val=real(cdwdisp);

		  double cdwo_val=real(cdwo);
		  
		  double nph_val=real(Nph);
		  double estruct_val=real(estruct);
		  double ekin_val=real(ekin);
    		    double avl2=real(c);
		     double avl3=real(curre);
		   
		    double elval=real(el);
		    double erval=real(er);

		    double e0val=real(e0);
		    double evval=real(ev);
		    double ecoupval=real(ecoup);

		    double ehyb_val=real(ehyb);
		    double max_bond=maxLinkDim(psi0);
		    double max_lbo=TE.maxLboDim();
       		    std::vector<double> v1;
       		    std::vector<double> v2;
       		    std::vector<double> v3;
       		    std::vector<double> v4;
       		    std::vector<double> v5;
       		    std::vector<double> v6;
       		    std::vector<double> v7;
       		    std::vector<double> v8;
       		    std::vector<double> v9;
       		    std::vector<double> v10;
		    std::vector<double> v11;
		    std::vector<double> v12;
		    std::vector<double> v13;
		    std::vector<double> v14;
		    std::vector<double> v15;
		    std::vector<double> v16;
		    std::vector<double> v17;
       		    v1.push_back(i*dt);
       		    v2.push_back(avl2);
       		    v3.push_back(cdwdisp_val);
       		    v4.push_back(cdwo_val);
       		    v5.push_back(estruct_val);
       		    v6.push_back(ne_val);
       		    v7.push_back(nph_val);
       		    v8.push_back(avl3);
       		    v9.push_back(erval);
       		    v10.push_back(elval);
		    v11.push_back(max_bond);
		    v12.push_back(max_lbo);
		    v13.push_back(ekin_val);
		    v14.push_back(ehyb_val);

		    v15.push_back(e0val);
		    v16.push_back(evval);
		    v17.push_back(ecoupval);
       		    auto vecSize=v1.size();
		    
       		    std::cout<< vecSize << "print "<<v1[0] <<std::endl;
       		    std::cout<<  "/time"+mpsName <<std::endl;
       		    itensor::ToFile(v1, DIR+"/time.dat",vecSize, true);
		    itensor::ToFile(v2, DIR+"/J.dat",vecSize, true);
       itensor::ToFile(v3, DIR+"/CDWdisp.dat",vecSize);
       itensor::ToFile(v4,DIR+ "/CDWO.dat",vecSize);
       itensor::ToFile(v5,DIR+ "/Estruct.dat",vecSize);
       itensor::ToFile(v6, DIR+"/Ne.dat",vecSize);
       itensor::ToFile(v7,DIR+"/Eph.dat",vecSize);
       itensor::ToFile(v8, DIR+"/Ecur.dat",vecSize);
       itensor::ToFile(v9,DIR+"/ER.dat",vecSize);
       itensor::ToFile(v10, DIR+"/EL.dat",vecSize);
       itensor::ToFile(v11, DIR+"/Bondmax.dat",vecSize);
       itensor::ToFile(v12, DIR+"/LBOmax.dat",vecSize);
       itensor::ToFile(v13, DIR+"/Ekinstruct.dat",vecSize);      
       itensor::ToFile(v14, DIR+"/Ehyb.dat",vecSize);      
       itensor::ToFile(v15, DIR+"/E0tot.dat",vecSize);      
       itensor::ToFile(v16, DIR+"/EVtot.dat",vecSize);      
       itensor::ToFile(v17, DIR+"/Ecoup.dat",vecSize);      

		  if(max_bond>=Md-100)
		    {

    std::cout<< "finish "<<std::endl;
return 0;
		    }
   
   		  std::cout<< std::setprecision(12)<<dt*i <<" Oder param "<<cdwo_val <<" ne "<< ne_val << " " <<c<< "  " << "  E "<< innerC(psir2, HV, psir2)<< " Order disp  "<< cdwdisp_val<<" " "lbo max "<< max_lbo<<"   "<< " mx  "<< max_bond<<" av  "<< averageLinkDim(psi0)<< "norm "<< NORM << " and H0 "<< (itensor::innerC(psir2, H0, psir2))<< "electron number "<< (itensor::innerC(psir2, Ntot, psir2))<<"\n";

		  		  TE.lbotDmrgStepStruct(Llead1, Lchain);
 

	   i++;
	       		 	  }


     return 0;
}
