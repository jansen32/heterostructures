
#include "itensor/all.h"
#include"files.hpp"
#include"holstein.hpp"
#include"lboclass.hpp"
#include"maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include <iomanip>
#include<utility>
#include <boost/program_options.hpp>


using namespace std;
using namespace itensor;

int main(int argc, char *argv[])
{

   std::string mpsName{};
 std::string siteSetName{};
  double cutoff{};
  double lbocutoff{};
  double norm_change;
  double phcutoff{};
  int M{};
  int Md{};
  int lboMd{};
  int L{};
  double t0{};
  double omega{};
  double gamma{};
  double dt{};
  double tot{};
  double V{};
    double eps0{};
  std::string scutoff{};
   std::string sphcutoff{};
  std::string slbocutoff{};
  std::string sM{};

  std::string sMd{};
  std::string slboMd{};
  std::string sL{};
  std::string st0{};
  std::string somega{};
  std::string sgamma{};
  std::string sdt{};
  std::string stot{};
  std::string sV{};
  std::string seps0{};
  std::string filename="";
std::string DIR="";
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
           ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(4), "M")
      ("Md", boost::program_options::value(&Md)->default_value(3000), "Md")
      ("lboMd", boost::program_options::value(&lboMd)->default_value(200), "lboMd")
      ("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
      ("gam", boost::program_options::value(&gamma)->default_value(1.0), "gam")
      ("dt", boost::program_options::value(&dt)->default_value(0.01), "dt")
      ("tot", boost::program_options::value(&tot)->default_value(1.0), "tot")
      ("V", boost::program_options::value(&V)->default_value(1.0), "V")
      ("eps", boost::program_options::value(&eps0)->default_value(1.0), "eps")
      ("lbocut", boost::program_options::value(&lbocutoff)->default_value(1E-15), "lbocut")
      ("cut", boost::program_options::value(&cutoff)->default_value(1E-15), "cut")
      ("d", boost::program_options::value(&DIR)->default_value("NODIR"), "d");

    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {      std::cout << "L: " << vm["L"].as<int>() << '\n';
      	sL="L"+std::to_string(vm["L"].as<int>());
 	filename+=sL;
      }
   
      	     if (vm.count("M"))
      {      std::cout << "M: " << vm["M"].as<int>() << '\n';
      	sM="M"+std::to_string(vm["M"].as<int>());
      	filename+=sM;
      }

 	           	     if (vm.count("Md"))
      {      std::cout << "Md: " << vm["Md"].as<int>() << '\n';
      	sMd="Md"+std::to_string(vm["Md"].as<int>());
      	filename+=sMd;
      }
 			           	     if (vm.count("M"))
      {      std::cout << "lboMd: " << vm["lboMd"].as<int>() << '\n';
      	slboMd="lboMd"+std::to_string(vm["lboMd"].as<int>());
      	filename+=slboMd;
      }
      	 if (vm.count("t0"))
      {      std::cout << "t0: " << vm["t0"].as<double>() << '\n';
      	st0="t0"+std::to_string(vm["t0"].as<double>()).substr(0, 3);
      	filename+=st0;
      }
 	 if (vm.count("gam"))
      {      std::cout << "gamma: " << vm["gam"].as<double>() << '\n';
      	sgamma="gam"+std::to_string(vm["gam"].as<double>()).substr(0, 3);
      	filename+=sgamma;
      }
      	 	 if (vm.count("omg"))
      {      std::cout << "omega: " << vm["omg"].as<double>() << '\n';
      	somega="omega"+std::to_string(vm["omg"].as<double>()).substr(0, 3);
      		filename+=somega;
      }
 		     	 	 if (vm.count("eps"))
      {      std::cout << "eps0: " << vm["eps"].as<double>() << '\n';
      	
      		filename+="eps0"+std::to_string(vm["eps"].as<double>()).substr(0, 3);
      }
      		 if (vm.count("V"))
      {      std::cout << "V: " << vm["V"].as<double>() << '\n';
      	sV="V"+std::to_string(vm["V"].as<double>()).substr(0, 3);
      		filename+=sV;
      }
      		 if (vm.count("tot"))
      {      std::cout << "tot: " << vm["tot"].as<double>() << '\n';
      	stot="tot"+std::to_string(vm["tot"].as<double>()).substr(0, 3);
      		filename+=stot;
      }
      		 if (vm.count("dt"))
      {      std::cout << "dt: " << vm["dt"].as<double>() << '\n';
      	sdt="dt"+std::to_string(vm["dt"].as<double>()).substr(0, 6);
      		filename+=sdt;
      }
      		 if (vm.count("cut"))
      {      std::cout << "cutoff: " << vm["cut"].as<double>() << '\n';
 	 std::stringstream ss;
 	 ss<<vm["cut"].as<double>();
 	 scutoff="cut"+ss.str();
      	filename+=scutoff;
      }
 		 if (vm.count("lbocut"))
 		   {
 		std::cout << "lbocutoff: " << vm["lbocut"].as<double>() << '\n';
 	 std::stringstream ss;
 	 ss<<vm["lbocut"].as<double>();
 	 slbocutoff="lbocut"+ss.str();
      	filename+=slbocutoff;
      }

 		       		 if (vm.count("d"))
      {      std::cout << "Dirname: " << DIR << '\n';

      }

      }
    }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  using Gate = BondGate;
   auto gates = vector<Gate>();
  auto sites = itensor::readFromFile<itensor::Holstein>(siteSetName);
  auto psi = itensor::readFromFile<MPS>(mpsName);
  size_t N=length(psi);
  std::vector<int> v(N, 0);
  
for(size_t i=0; i<N; i++)
    {
      v[i]=M;

    }//"MinDim=",maxLinkDim(psi0)
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};
  auto argsMPS = itensor::Args("Cutoff=",cutoff,"MaxDimLBO",lboMd, "CutoffLBO",lbocutoff  , "MaxDim=",Md, "Normalize",true, "SVDMethod", "ITensor");


 psi.position(1);
    // Print(psi.A(1));
     psi.normalize();
 


 std::map<std::string, double> param;
 param.insert({"t0", t0});
 param.insert({"gamma", gamma});
  param.insert({"omega", omega});
  param.insert({"Const", eps0});
  itensor::IHG GM(sites, gates, dt, param);


 TimeEvolveLbo< IHG, decltype(psi), false> C(GM,  psi, L,  argsMPS, argsState); 
  C.makeLbo();
  std::cout<< "start "<<std::endl;
  GM.makeGates_V(V);
 std::vector<double> obstebdlbo;
  std::vector<double> time;

    std::chrono::duration<double> elapsed_seconds;
    std::chrono::time_point<std::chrono::system_clock> start, end;//  C.TEBDStepO2();
    auto CDWdisp=CDWDispOparam(sites);

auto CDWO=CDWOparam(sites);
 
	        int i=0;
//     std::cout<< "j2j2j "<< std::endl;
     while(i*dt<tot)
     {
             start = std::chrono::system_clock::now(); 
	  

      	 end = std::chrono::system_clock::now();
      	 		  
      				auto psir2=C.getBareMps();
      	    psir2.position(1);
 
       	  auto NORM=itensor::norm(psir2);



  std::vector<double> v1;
       		    std::vector<double> v2;
       		    std::vector<double> v3;
       		    std::vector<double> v4;
       		    std::vector<double> v5;

			    auto cdwdisp = (itensor::innerC(psir2, CDWdisp, psir2));
			    auto cdwo = (itensor::innerC(psir2, CDWO, psir2));
			    	  double cdwdisp_val=real(cdwdisp);

double cdwo_val=real(cdwo);
 v1.push_back(i*dt);
 v2.push_back(cdwdisp_val);
v3.push_back(cdwo_val);
 v4.push_back(C.maxLboDim());
v5.push_back(maxLinkDim(psi));
 for(int j=1; j<=L; j++){

   	std::vector<double> nevec2;
			std::vector<double> nphvec;
			psir2.position(j);
			  auto ket = psir2(j);
        auto bra = dag(prime(ket,"Site"));
        auto Neloc = op(sites,"n",j);
	auto Nphloc = op(sites,"Nph",j);
	auto nph = eltC(bra*Nphloc*ket);
	auto ne = eltC(bra*Neloc*ket);
	nevec2.push_back(real(ne));
	nphvec.push_back(real(nph));
	auto vecSize=nevec2.size();
	itensor::ToFile(nphvec, DIR+"/Nphloct"+std::to_string(j)+".dat", vecSize);
	itensor::ToFile(nevec2,DIR+"/Neloct"+std::to_string(j)+".dat", vecSize);
		      }
 
       		   
 auto vecSize=v1.size();
 itensor::ToFile(v1, DIR+"/time.dat",vecSize);
        itensor::ToFile(v3, DIR+"/CDWO.dat",vecSize);
        itensor::ToFile(v2, DIR+"/CDWdisp.dat",vecSize);
        itensor::ToFile(v4, DIR+"/LBOmax.dat",vecSize);
        itensor::ToFile(v5, DIR+"/Bondmax.dat",vecSize);
 	  std::cout<<std::setprecision(8)<<"NO "<<norm(psir2)<<"  "<<cdwo<< "  "<< i*dt<< "  max lbo "<< C.maxLboDim()<<"  "<<'\n';
  
	     C.lbotDmrgStep();
	i+=1;
     }

  
  return 0;
}
