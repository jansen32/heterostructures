#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"timeevclass.hpp"
#include"lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"htham.hpp"
#include <boost/program_options.hpp>
#include<iostream>

using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{

  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();


  int Llead1{};
  int Llead2{};
  int Lchain{};
  int M;
  std::string filename;
  double gamma{};
std::string mpsName{};
 std::string siteSetName{};
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("Ll1", boost::program_options::value(&Llead1)->default_value(4), "Ll1")
      ("Ll2", boost::program_options::value(&Llead2)->default_value(4), "Ll2")
      ("Lc", boost::program_options::value(&Lchain)->default_value(4), "Lc")
      ("gam", boost::program_options::value(&gamma)->default_value(0), "gam")
      ("f", boost::program_options::value(&filename)->default_value("noName"), "f")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName");
    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("Ll1"))
      {     
	// std::cout << "Llead1: " << Llead1 << '\n';

      }
            if (vm.count("Ll2"))
      {   
	//   std::cout << "Llead2: " << Llead2 << '\n';

      }
         if (vm.count("Lc"))
      {   
	//   std::cout << "Lchain: " << Lchain << '\n';

      }
      	     if (vm.count("M"))
      {   
	//   std::cout << "M: " << M << '\n';

      }

    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	
  int N=Llead1+Llead2+Lchain;
  
  std::vector<int> v(N, 0);
  for(int i=Llead1; i<Llead1+Lchain; i++)
    {
      v[i]=M;

    }
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};

  auto sites = itensor::readFromFile<itensor::Holstein>(siteSetName);
  auto psi = itensor::readFromFile<MPS>(mpsName);

    auto n_l= AutoMPO(sites);
    auto n_r= AutoMPO(sites);
    auto n_s= AutoMPO(sites);
    for(int i=1; i<=Llead1; i++)
      {
	n_l+=1,"n",i;
}

    for(int i=Llead1+1; i<=Llead1+Lchain; i++)
      {
	n_s+=1,"n",i;
}


    for(int i=Llead1+Lchain+1; i<=N; i++)
      {
	n_r+=1,"n",i;
}
    auto N_L=toMPO(n_l);
    auto N_S=toMPO(n_s);
    auto N_R=toMPO(n_r);
    auto N_Lev=inner(psi, N_L, psi);
    auto N_Sev=inner(psi, N_S, psi);
    auto N_Rev=inner(psi, N_R, psi);
  auto CDWdisp=CDWDispstruct(sites, Llead1, Lchain);
auto CDWO=CDWOstruct(sites, Llead1, Lchain);
    auto cdwoev=inner(psi, CDWO, psi);
    auto cdwdispev=inner(psi, CDWdisp, psi);

  std::cout<<"total "<< (N_Rev+N_Sev+N_Lev)/N << " N_l "<< N_Lev/Llead1<< " N_s "<< N_Sev/Lchain<< " N_r "<< N_Rev/Llead2<<std::endl;

  std::cout<<"cdw o "<< cdwoev << " cdwdisp "<< cdwdispev<<std::endl;
    std::ofstream outfile;

    outfile.open(filename, std::ios_base::app); // append instead of overwrite
    outfile <<std::endl<<std::fixed<<std::setprecision(3)<< gamma<<","<<cdwoev<<","<<cdwdispev; 
    outfile.close();
     return 0;
}
