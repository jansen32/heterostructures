#include <iostream>
#include <cmath>
#include "itensor/all.h"

using namespace itensor;
using namespace std;

int main(int argc, char *argv[])
{
  int n=3;
  auto ind = Index(n);


  auto T = ITensor(ind,prime(ind));
  // for(int i=1; i<=3; i++)
//     {
//       T.set(ind(i), prime(ind(i)), i);
// }
  // T.set(ind(1), prime(ind(2)), std::complex<double>{0,1});
  // //  T.set(ind(1), prime(ind(1)), std::complex<double>{3,0});
  // T.set(ind(2), prime(ind(1)), -std::complex<double>{0,1});

  // T.set(ind(1), prime(ind(3)), std::complex<double>{0,1});
  // T.set(ind(2), prime(ind(3)), std::complex<double>{0,1});
  // //  T.set(ind(1), prime(ind(1)), std::complex<double>{3,0});
  // T.set(ind(3), prime(ind(1)), -std::complex<double>{0,1});
  // T.set(ind(3), prime(ind(2)), -std::complex<double>{0,1});
  // //Make Hermitian tensor out of T

  T.set(ind(1), prime(ind(2)), std::complex<double>{1,0});
  //  T.set(ind(1), prime(ind(1)), std::complex<double>{3,0});
  T.set(ind(2), prime(ind(1)), -std::complex<double>{1,0});

  T.set(ind(1), prime(ind(3)), std::complex<double>{1,0});
  T.set(ind(2), prime(ind(3)), std::complex<double>{1,0});
  //  T.set(ind(1), prime(ind(1)), std::complex<double>{3,0});
  T.set(ind(3), prime(ind(1)), -std::complex<double>{1,0});
  T.set(ind(3), prime(ind(2)), -std::complex<double>{1,0});
  //  T*=-1;
  auto [U,D] = diagHermitian(T, {"ShowEigs", true}); 
  auto u = commonIndex(U,D);

  print(hasInds(D,{u,prime(u)})); //prints: true
  std::cout<<std::endl;  
  print(norm(T-dag(U)*D*prime(U))); //prints on the order of 1E-15
  std::cout<<std::endl;  
print(U);
   for(int i=1; i<=n; i++)
     {
       std::cout<<elt(D,u(i), prime(u(i)))<<std::endl;
}
   std::cout<<std::endl;
   auto inds_v=inds((U));
   print(inds_v[0]);
   print(inds_v[1]);
   std::cout<<endl;
 for(int i=1; i<=n; i++)
   	{
   //   for(int j=1; j<=n; j++)
   //	{
   int j=1;
 	  std::cout<< (U).eltC(inds_v[0](i), inds_v[1](j))<<"   ";
 //}
   std::cout<< endl;
	}
    return(0);
}
