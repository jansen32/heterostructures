#include "itensor/all.h"
#include"files.hpp"
#include"vidalnot.hpp"
#include"holstein.hpp"
#include"maketimeevop.hpp"
#include"makehamiltonians.hpp"
#include <iostream>
#include"timeevclass.hpp"
#include"lboclass.hpp"
#include <iomanip>
#include <cmath>
#include"htham.hpp"
#include <boost/program_options.hpp>
#include<iostream>

using namespace std;

// using namespace boost::program_options;
int main(int argc, char *argv[])
{

  using itensor::MPO;
  using itensor::MPS;
  using itensor::AutoMPO;
    using Gate = itensor::BondGate;
 auto gates = vector<Gate>();


  int L{};

  int M;
  double gamma{};

  double t0{};
  double omega{};
    double omegap{};

double eps{};
std::string mpsName{};
 std::string siteSetName{};
std::string filename{};
  
  try
  {
    boost::program_options::options_description desc{"Options"};
    desc.add_options()
      ("help,h", "Help screen")
      ("L", boost::program_options::value(&L)->default_value(4), "L")
      ("M", boost::program_options::value(&M)->default_value(0), "M")
      ("gam", boost::program_options::value(&gamma)->default_value(0), "gam")
("t0", boost::program_options::value(&t0)->default_value(1.0), "t0")
      ("omg", boost::program_options::value(&omega)->default_value(1.0), "omg")
            ("eps", boost::program_options::value(&eps)->default_value(0), "eps")
      ("f", boost::program_options::value(&filename)->default_value("noName"), "f")
      ("mpsN", boost::program_options::value(&mpsName)->default_value("noName"), "mpsN")
      ("siteN", boost::program_options::value(&siteSetName)->default_value("noName"), "siteSetName");
    boost::program_options::variables_map vm;
    boost::program_options::store(parse_command_line(argc, argv, desc), vm);
    boost::program_options::notify(vm);
    
    if (vm.count("help"))
      {std::cout << desc << '\n'; return 0;}
    else{
      if (vm.count("L"))
      {     
	// std::cout << "Llead1: " << Llead1 << '\n';

      }

      	     if (vm.count("M"))
      {   
	//   std::cout << "M: " << M << '\n';

      }

    }
  }
  catch (const boost::program_options::error &ex)
  {
    std::cerr << ex.what() << '\n';
    return 0;
  }
  	
  int N=L;
  
  std::vector<int> v(N, M);
  // for(int i=0; i<N; i++)
  //   {
  //     v[i]=M;

  //   }
  itensor::Args argsState={"ConserveNf=",true,
                             "ConserveNb=",false,
			   "DiffMaxOcc=",true, "MaxOccVec=", v};

  auto sites = itensor::readFromFile<itensor::Holstein>(siteSetName);
  auto psi = itensor::readFromFile<MPS>(mpsName);

    auto n= AutoMPO(sites);

    for(int i=1; i<=L; i++)
      {
	n+=1,"n",i;
}


    auto N_e=toMPO(n);

    auto CDWdisp=toMPO(CDWDispOparam(sites));
    auto CDWO=toMPO(CDWOparam(sites));
    auto cdwoev=inner(psi, CDWO, psi);
    auto cdwdispev=inner(psi, CDWdisp, psi);

    //    std::cout<<"total "<< inner(psi, N_e, psi)/N << std::endl;

    //std::cout<<"cdw o "<< cdwoev << " cdwdisp "<< cdwdispev<<std::endl;
      auto am2=makeHolstHam(sites, t0, gamma, omega, eps);
      auto H2 = toMPO(am2);
      auto E0=itensor::innerC(psi, H2,psi)/itensor::innerC(psi, psi);
  std::cout<< "energy "<< E0<<std::endl;

 auto args = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000, "Normalize", false);
 auto args2 = itensor::Args("Cutoff=",1e-13,"MaxDim=",3000);

   auto psiOne=applyMPO(H2,psi, args);
   psiOne.noPrime();
//auto H1sqrd=itensor::multSiteOps(H2, H2);
   auto E0Sqrd=itensor::innerC(psi,H2,psiOne);
   //std::cout<< " djei "<< E0Sqrd<<std::endl;
  	  auto VAL=E0*E0;
//  std::cout<< " djei 2 2 "<< VAL<<std::endl;
  auto relVARIANCE =  std::abs((E0Sqrd-VAL )/VAL);
  		    std::cout<< "THE REL GS VAR WAS "<< relVARIANCE<<std::endl;
  		    std::cout<< "THE GS VAR WAS "<< std::abs(E0Sqrd-VAL )<<std::endl;  
    std::ofstream outfile;
    outfile.open(filename, std::ios_base::app); // append instead of overwrite
    outfile <<std::endl<< gamma<<","<<E0<<","<<relVARIANCE; 
    outfile.close();
     return 0;
}
