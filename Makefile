LIBRARY_DIR=$(ITENSOR)
include $(LIBRARY_DIR)/this_dir.mk
include $(LIBRARY_DIR)/options.mk
INC+=-I${ITENSOR}
INC+=-I${TENSORFT}/include/
INC+=-I${TENSORTIMEEV}/include/
INC+=-I${TENSOR}/TDVP/
INC+=-I${TENSOR}/parallel_tdvp_lbo
INC+=-I${TENSORTOOLS}/include
INC+=-I${MANYBODY}/include

INC+=-I${EINC}/include/boost/



LIBSLINK+=-L${ELIBS}
LIBSPATH=-L$(ITENSOR)/lib
LIBSPATH+=$(LIBSLINK)
LIBS=-litensor -lboost_program_options
LIBSG=-litensor-g -lboost_program_options

CCFLAGS+=-I. $(ITENSOR_INCLUDEFLAGS) $(OPTIMIZATIONS) -Wno-unused-variable -std=c++17 -O2 -std=gnu++1z
CCGFLAGS+=-I. $(ITENSOR_INCLUDEFLAGS) $(DEBUGFLAGS) 

LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -L${ELIBS}/ -lboost_program_options 
LIBGFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBGFLAGS) -lboost_program_options 
MPICOM=mpicxx -m64 -std=c++17 -fconcepts -fPIC

#LIBFLAGS=-L$(ITENSOR_LIBDIR) $(ITENSOR_LIBFLAGS) -Wl,--start-group /home/jansen32/usr/lib2/lib/libboost_program_options.a /home/jansen32/usr/lib2/lib/libboost_filesystem.a -Wl,--end-group 

CPPFLAGS_EXTRA += -O2 -std=c++17

DB=-g
CXX=g++
ND=-DNDEBUG

#################################
# ED code 
###############################
INC_ED=-I${MANYBODY}/include
INC_ED+=-I${EIGEN}
INC_ED+=-I${EINC}
LIBSLINK_ED=-L${MANYBODY}/libs -L${ELIBS}
LIBS_ED=-leigenmkl32 -lboost_program_options
MKLLINK_ED=-DMKL_Complex16="std::complex<double>" -m64 -I${MKLROOT}/include -L${MKLROOT}/lib/intel64 -Wl,--no-as-needed -lmkl_intel_lp64 -lmkl_gnu_thread -lmkl_core -lgomp -lpthread -lm -ldl

#########################

##################################################
holwVtdmrglbo: src/holwVtdmrglbo.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

struct_GS: src/struct_GS.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

struct_GS_2: src/struct_GS_2.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
struct_GS_3: src/struct_GS_3.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
struct_GS_4: src/struct_GS_4.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

struct_GS_in2: src/struct_GS_in2.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

structtdmrglbo: src/structtdmrglbo.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

holGS: src/holGS.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)
holGS_3: src/holGS_3.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

edtest: src/edtest.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

elec_disStruct: src/elec_disStruct.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

elec_disHol: src/elec_disHol.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

convStruct: src/convStruct.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

convHol: src/convHol.cpp $(ITENSOR_LIBS)
	$(CCCOM)  $< -o bin/$@ $(CCFLAGS) $(INC) $(LIBFLAGS)

########################################
# ED
hetFerexSP: src/hetFerexSP.cpp
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)

holsttimeexactpol: src/holsttimeexactpol.cpp 
	$(CXX) $(FLAGS) $(INC_ED) $< -o bin/$@  $(MKLLINK_ED) $(LIBS_ED) $(LIBS_ED) $(LIBSLINK_ED) $(ND) $(OP)

###############################

clean:
	rm bins/* 

