#!/usr/bin/env python
import os
import numpy as np

dV_arra=[1., 4, 5]
gam_arra=np.arange(6, 14, 1)

gam_arra=np.sqrt(gam_arra)
gam_arra*=0.5
dt=0.05
#tot=20.
print("gam ", gam_arra)
tot=20
pb=0
M=50
L=2
t0=0.5
omg=0.5

print(gam_arra)
binar="holsttimeexactpol"
for dV in dV_arra:
    for gam in gam_arra:
        eps=gam**2/omg
        dirname="EDDATA/holst_pol_L{0}M{1}tot{2}dt{3}t0{4}gam{5:.3f}omg{6:.3f}eps{7:.3f}dV{8:.3f}pb{9}DIR".format(L,M,tot,dt,t0,gam,omg, eps,dV, pb)
        command="MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=2 ./bin/{0} --dir {10} --L {1} --M {2} --tot {3} --dt {4} --t {5} --omg {6}  --gam {7} --eps {8} --pb {9} --DV {11}".format( binar, L, M, tot, dt, t0, omg, gam,eps, pb, dirname, dV)
        os.mkdir(dirname)
        comm_file=open(dirname+"/command.txt","w")
        comm_file.write(command)
        comm_file.close()
        print(command)
        os.system(command)
    
os.system("echo Hello from the other side!")



#!/usr/bin/env python
#import os
#import numpy as np


# dt=0.1
# tot=20.
# pb=0
# M=50
# L=2
# t0=1.
# DV=
# omg=0.1
# gam=-0.4

# binar="holsttimeexactpol"
# dirname="holst_pol_L{0}M{1}tot{2}dt{3}t0{4}gam{5:.3f}omg{6:.3f}pb{7}".format(L,M,tot,dt,t0,gam,omg, pb)
# command="MKL_NUM_THREADS=4 ./{0} --dir {9} --L {1} --M {2} --tot {3} --dt {4} --t {5} --omg {6}  --gam {7} --pb {8}".format( binar, L, M, tot, dt, t0, omg, gam, pb, dirname)
# os.mkdir(dirname)
# print(command)
# os.system(command)
    


# omg=2.
# gam=-4.

# binar="holsttimeexactpol"
# dirname="holst_pol_L{0}M{1}tot{2}dt{3}t0{4}gam{5:.3f}omg{6:.3f}pb{7}".format(L,M,tot,dt,t0,gam,omg, pb)
# command="MKL_NUM_THREADS=4 ./{0} --dir {9} --L {1} --M {2} --tot {3} --dt {4} --t {5} --omg {6}  --gam {7} --pb {8}".format( binar, L, M, tot, dt, t0, omg, gam, pb, dirname)
# os.mkdir(dirname)
# print(command)
# os.system(command)

