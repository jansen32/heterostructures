
#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

M="50"

L1=113
L2=114



Lc=9


#dg=0.25/2
#gam=0.25+dg
gam=np.sqrt(6)*0.5
omg=0.5
tl=1.0
thyb=0.5
t0=0.5
eps=gam**2/omg
MS=400
filename="GS_2structL1{0}L2{1}Lc{2}M{3}gam{4:.3f}omg{5}eps{6}t0{7}tl{8}tint{9}MS{10}.txt".format(L1,L2,Lc,M,gam, omg, eps, t0, tl,thyb,MS)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./bin/struct_GS_2 --Ll1 {0} --Ll2 {1} --Lc {2} --M {3} --gam {4} --omg {5} --eps {6}  --t0 {7} --tl {8} --tint {9}  --MS {10} --SV 1 | tee {11}".format(L1,L2,Lc,M,gam, omg, eps, t0, tl,thyb, MS,filename )



comm_list=shlex.split(comm)
#print(comm_list)

#['echo', 'More output'],
#process = subprocess.Popen(comm_list,shell=True,
 #                    stdout=subprocess.PIPE, 
  #                   stderr=subprocess.PIPE)
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout)

