#!/bin/bash
gam=3.0
#$(awk -v x=2. 'BEGIN{print sqrt(x)}')
t0=1
omg=1.
eps=$(echo "$gam * $gam / $omg" | bc)
#$(awk -v x=${gam} 'BEGIN{print x*x/$omg}')
cut=1e-09
lbocut=1e-09


L=10
M=20

OMP_NUM_THREADS=4 ./bin/holGS --L ${L} --M ${M} --omg ${omg} --t0 ${t0} --gam ${gam} --eps ${eps} --MS 200 --SS 1
