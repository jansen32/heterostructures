
#!/bin/bash
M="1"
lboMd="100" 
L1=10
L2=11



Lc=9
cut="1e-08"
Md="1500"
#L=60
gam=0.
omg=0.5
thyb=0.5
t0=0.5
eps=$(echo "$gam * $gam / $omg" | bc)


echo $gam




time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./holdotGS --Ll1 ${L1} --Ll2 ${L2} --Lc ${Lc} --M ${M} --gam ${gam} --omg ${omg} --eps ${eps}  --lboMd 200 --lbocut 1e-09 --cut 1e-09 --t0 ${t0} --tl 1 --tint ${thyb}  --MS 100 --SV 1

