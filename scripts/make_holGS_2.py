#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

M="50"

L=13



gam=np.sqrt(2)
#gam=np.sqrt(13)*0.5
##np.sqrt(2)
omg=0.5

t0=0.5
eps=gam**2/omg
MS=1000
filename="GShol_2L{0}M{1}gam{2:.3f}omg{3}eps{4}t0{5}MS{6}.txt".format(L,M,gam, omg, eps, t0,MS)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./bin/holGS_2 --L {0}  --M {1} --gam {2} --omg {3} --eps {4}  --t0 {5}  --MS {6} --SS 1 | tee {7}".format(L,M,gam, omg, eps, t0,MS,filename )



comm_list=shlex.split(comm)
#print(comm_list)

#['echo', 'More output'],
#process = subprocess.Popen(comm_list,shell=True,
 #                    stdout=subprocess.PIPE, 
  #                   stderr=subprocess.PIPE)
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout)

