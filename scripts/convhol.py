#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess



gam_arr=np.arange(0,2.52, 0.25)
#gam_arr=[np.sqrt(2)]
gam_arr=np.append(np.sqrt(2),gam_arr)
gam_arr=np.sort(gam_arr)
#p.sqrt(2)

omg=0.5

t0=0.5


cut=1e-08
lbocut=1e-07
L=5
M=35
MS=1000

exe="convHol"
filename='GSOdpar/holconvL{0}M{1}MS{2}.csv'.format(L,M,MS) 
with open(filename, 'w') as filetowrite:
    filetowrite.write('#gam,E0,relV')
for gam in gam_arr:
    eps=gam**2/omg
    print(len(str(gam)), str(gam))
    if(len(str(gam))<5):
        print("GGG")
        sg="{0:.3f}".format(gam)
        print(sg)
    else:
        print("CC")
        sg=str(gam)[0:5]
        print(sg)
    if(len(str(eps))<4):
        se="{0:.3f}".format(eps)

    else:
        se="{0:.5f}".format(eps)[0:5]
    name="GSdmrgL{0}M{1}MS{2}t0{3:.3f}gam{4}omega{5:.3f}eps{6}.bin".format(L,M,MS,t0, sg, omg, se)
    mpsN="HOLSTATES/MPS"+name
    siteN="HOLSTATES/siteset"+name





    comm="MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4   ./bin/{0}  --L {1}  --M {2}  --mpsN {3} --siteN {4} --gam {5} --t0 {6} --omg {7}  --f {8}".format(exe, L, M, mpsN, siteN, gam, t0 , omg, filename)
#    print(comm)
    print("gamma =",gam, "eps ",eps )
    stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
with open(filename, 'a') as filetowrite:
    filetowrite.write('\n')
