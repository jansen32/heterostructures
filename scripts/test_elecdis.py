#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
Lc=13
L1=111
L2=112

gam_arr=np.arange(0,2.52, 0.25)
#gam_arr=[np.sqrt(2)]
gam_arr=np.append(np.sqrt(2),gam_arr)
gam_arr=np.sort(gam_arr)

omg=0.500




t0=0.500
thyb=0.500
tl=1.000

MS=200
M=35
exe="elec_disStruct"
filename='GSOdpar/structLc{0}M{1}MS{2}.csv'.format(Lc,M,MS) 
with open(filename, 'w') as filetowrite:
    filetowrite.write('#gam,cdwn,cdwx')
for gam in gam_arr:
    eps=gam**2/omg
    if(len(str(gam))<4):
        sg="{0:.3f}".format(gam)

    else:
        sg="{0:.5f}".format(gam)[0:5]

    if(len(str(eps))<4):
        se="{0:.3f}".format(eps)

    else:
        se="{0:.5f}".format(eps)[0:5]



    name="holstructGSLl1{0}Ll2{1}Lc{2}M{3}MS{4}t0{5:.3f}tl{6:.3f}tint{7:.3f}gam{8}omega{9:.3f}eps0{10}.bin".format(L1, L2, Lc, M,MS,t0, tl, thyb, sg, omg, se )
    mpsN="STRUCTSTATES/MPS"+name
    siteN="STRUCTSTATES/siteset"+name





    comm="MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4   ./bin/{0}  --Ll1 {1} --Ll2 {2} --Lc {3} --M {4}  --mpsN {5} --siteN {6} --f {7} --gam {8}".format(exe, L1, L2, Lc, M, mpsN, siteN, filename, gam)

    print("gamma =",gam, "eps ",eps )
    stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
