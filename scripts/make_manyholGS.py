#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

M="30"

L=9



#gam_arr=np.arange(0, 1.26, 0.25)
#gam_arr=np.arange(1.5, 2.54, 0.25)
omg=0.5

t0=0.5
MS=1000
for g in gam_arr:
    eps=g**2/omg
    filename="GSholL{0}M{1}gam{2:.3f}omg{3}eps{4}t0{5}MS{6}.txt".format(L,M,g, omg, eps, t0,MS)
    comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./bin/holGS --L {0}  --M {1} --gam {2} --omg {3} --eps {4}  --t0 {5}  --MS {6} --SS 1 | tee {7}".format(L,M,g, omg, eps, t0,MS,filename )
#    comm_list=shlex.split(comm)
    stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout)

