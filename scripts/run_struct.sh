#!/bin/bash
Lc=1
L1=117
L2=118

gam=0.5
#$(awk -v x=4.0 'BEGIN{print sqrt(0.5*x)}')
gams="0.250"
eps="0.125"


cut=1e-08
lbocut=1e-07

M=30
omg="0.500"

t0="0.500"
thyb="0.500"
mpsN="MPSholdotGSLl1${L1}Ll2${L2}Lc${Lc}M${M}t0${t0}tl1.000tint${thyb}gam${gams}omega${omg}eps0${eps}.bin"
siteN="sitesetholdotGSLl1${L1}Ll2${L2}Lc${Lc}M${M}t0${t0}tl1.000tint${thyb}gam${gams}omega${omg}eps0${eps}.bin"
dir="DOTGSSTATES/"
tot=75.
theta=0
V=0.1
#Md="1500"
#exe=holdotparalbo
exe=holdottdmrglbo

time OMP_NUM_THREADS=4 MKL_NUM_THREADS=1 ./${exe} --Lc ${Lc} --Ll1 ${L1} --Ll2 ${L2} --M ${M} --tint ${thyb}  --tot ${tot} --dt 0.05 --cut ${cut} --lbocut ${lbocut} --gam ${gam} --omg ${omg} --t0 ${t0} --eps ${eps} --V ${V} --mpsN ${dir}/${mpsN} --siteN ${dir}/${siteN} --theta ${theta} |& tee ${exe}L1${L1}L2${L2}Lc${Lc}gam${gam}M${M}t0${t0}omg${omg}cut${cut}omgp${omgp}dt${dt}V${V}theta${theta}.txt
