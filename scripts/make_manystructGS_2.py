#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
import shlex

M="50"

L1=115
L2=116



Lc=5


#gam_arr=np.arange(0., 1.06, 0.25)
gam_arr=np.arange(1.25, 2.54, 0.25)

omg=0.5
tl=1.0
thyb=0.5
t0=0.5

MS=200
for g in gam_arr:
    eps=g**2/omg
    filename="GS_2structL1{0}L2{1}Lc{2}M{3}gam{4:.3f}omg{5}eps{6}t0{7}tl{8}tint{9}MS{10}.txt".format(L1,L2,Lc,M,g, omg, eps, t0, tl,thyb,MS)
    comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./bin/struct_GS_2 --Ll1 {0} --Ll2 {1} --Lc {2} --M {3} --gam {4} --omg {5} --eps {6}  --t0 {7} --tl {8} --tint {9}  --MS {10} --SV 1 | tee {11}".format(L1,L2,Lc,M,g, omg, eps, t0, tl,thyb,MS, filename )

    stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout)

