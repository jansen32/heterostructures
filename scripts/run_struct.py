#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess
Lc=9
L1=113
L2=114

gam=0.75
#np.sqrt(2)
#0.375
#np.sqrt(2)

omg=0.500
eps=gam**2/omg



t0=0.500
thyb=0.500
tl=1.000
cut=1e-08
lbocut=1e-07
MS=200
M=35
if(len(str(gam))<4):
    sg="{0:.3f}".format(gam)

else:
    sg="{0:.5f}".format(gam)[0:5]
if(len(str(eps))<4):
    se="{0:.3f}".format(eps)

else:
    se="{0:.5f}".format(eps)[0:5]


name="holstructGSLl1{0}Ll2{1}Lc{2}M{3}MS{4}t0{5:.3f}tl{6:.3f}tint{7:.3f}gam{8}omega{9:.3f}eps0{10}.bin".format(L1, L2, Lc, M,MS,t0, tl, thyb, sg, omg, se )
mpsN="STRUCTSTATES/MPS"+name
siteN="STRUCTSTATES/siteset"+name

tot=75.
dt=0.05
V=0.1
saveBasis=0
exe="structtdmrglbo"
dirName="DATA/structGSLl1{0}Ll2{1}Lc{2}M{3}MS{4}t0{5:.3f}tl{6:.3f}tint{7:.3f}gam{8}omega{9:.3f}eps0{10:.3f}cut{11}lbocut{12}tot{13}dt{14:.3f}V{15:.3f}DIR".format(L1, L2, Lc, M,MS,t0, tl, thyb, sg, omg, eps , cut, lbocut, tot, dt, V)
print(dirName)
if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirName)

comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4   ./bin/{0}  --Ll1 {1} --Ll2 {2} --Lc {3} --M {4} --t0 {5} --tl {6} --tint {7} --gam {8} --omg {9}  --eps {10}  --cut {11} --lbocut {12}  --tot {13} --dt {14}  --V {15} --mpsN {16} --siteN {17} --saveBasis {18} --d {19} ".format(exe, L1, L2, Lc, M,t0, tl, thyb, gam, omg, eps , cut, lbocut, tot, dt, V, mpsN, siteN, saveBasis, dirName)

comm_file=open(dirName+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
