
#!/usr/bin/python
import sys
import numpy as np
import os
import subprocess



#gam=np.sqrt(2)
gam=np.sqrt(6)*0.5

omg=0.5
eps=gam**2/omg
t0=0.5


cut=1e-08
lbocut=1e-08
L=9
M=50
MS=1000
if(len(str(gam))<4):
    sg="{0:.3f}".format(gam)
else:
    sg=str(gam)[0:5]
name="GSdmrgL{0}M{1}MS{2}t0{3:.3f}gam{4}omega{5:.3f}eps{6:.3f}.bin".format(L,M,MS,t0, sg, omg, eps)
mpsN="HOLSTATES/MPS"+name
siteN="HOLSTATES/siteset"+name
tot="75."
dt=0.05

print(mpsN)
#V=10.000
V=50.000
#V=10.000

#print(mpsN[:-4])
dirName="DATA/holL{0}M{1}MS{2}t0{3:.3f}gam{4:.3f}omega{5:.3f}eps{6:.3f}cut{7}lbocut{8}tot{9}dt{10:.3f}V{11:.3f}DIR".format(L,M,MS,t0, gam, omg, eps, cut, lbocut,tot,dt, V)
print(dirName)
if(os.path.isdir(dirName)):
    dirName+="2"

if(os.path.isdir(dirName)):
    dirName+="2"
if(os.path.isdir(dirName)):
    print("to many versions of dirName exist")
    sys.exit()
os.mkdir(dirName)
comm="time MKL_THREADING_LAYER=sequential OMP_NUM_THREADS=4 ./bin/holwVtdmrglbo --L {0} --M {1}  --t0 {2} --gam {3} --omg {4}  --eps {5} --cut {6} --lbocut {7} --tot {8} --dt {9}  --V {10} --mpsN {11} --siteN {12} --d {13}/".format(L,M,t0, gam, omg, eps, cut, lbocut,tot,dt, V, mpsN, siteN, dirName)
comm_file=open(dirName+"/command.txt","w")
comm_file.write(comm)
comm_file.close()
stdout=subprocess.check_call(comm, shell=True, stdout=sys.stdout, stderr=sys.stderr)
#
