#!/bin/bash
gam=$(awk -v x=3.0 'BEGIN{print sqrt(0.5*x)}')
gams="1.224"
eps="3.000"


cut=1e-08
lbocut=1e-07
L=9
M=35
omg="0.500"

t0="0.500"
mpsN="MPSGSdmrgL${L}M${M}MS200t0${t0}gam${gams}omega${omg}eps${eps}.bin"
siteN="sitesetGSdmrgL${L}M${M}MS200t0${t0}gam${gams}omega${omg}eps${eps}.bin"
tot=60



#V=10.
#V=40.
V=50.
Md="1500"
time OMP_NUM_THREADS=2 MKL_NUM_THREADS=1 ./timeholtdmrglbowV --L ${L} --M ${M}  --tot ${tot} --dt 0.05 --cut ${cut} --lbocut ${lbocut} --gam ${gam} --omg ${omg} --t0 ${t0} --eps ${eps} --V ${V} --mpsN ${mpsN} --siteN ${siteN} 
